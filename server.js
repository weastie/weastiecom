// Lets me use my lets and consts
'use strict';

// Imports
global.essentials = require('./essentials.js');
// Manage the HTTP server
var express = require('express');
var app = express();
// Compress and speed up data transfer
var compression = require('compression');
// Make sure that important errors WILL crash the server
var assert = require('assert');
// Communicate with mongodb server
var mongoClient = require('mongodb').MongoClient;
// Store mongo collection data here
var mCols = {};
// Calculations with mongodb storage size
var BSON = require('bson');
var bson = new BSON();
// Post data to other servers for verification
var request = require('request');
// Read unencrypted body data
var bodyParser = require('body-parser');
// Read forms with files
var formidable = require('formidable');
// enables the req.session variable in express
var session = require('express-session');
// Manage file system
var fs = require('fs');
var path = require('path');
global.path = path;

var https = require('https');

// To run system commands
var exec = require('child_process').exec;

// Captcha key
var secretData = {
	captchaKey: '',
	adminPW: ''
};

// Compile less css code
exec('lessc assets/css/pre-compile/main.less assets/css/compiled/main.css');

// Determine whether or not we are in production mode through an environment variable
var PRODUCTION = (process.env.PRODUCTION === 'true');
console.log('PRODUCTION MODE: ' + PRODUCTION);

// Middleware and express configuration
app.use(bodyParser.urlencoded({extended: false, limit: '80kb'}));
app.use(compression());
app.set('port', PRODUCTION ? 443 : 3000); // 443 (https) for production, 3000 for testing
app.set('view engine', 'pug');
app.set('views', './views');
app.use('/assets', express.static('./assets'));

// Lets encrypt verification
app.use('/.well-known/acme-challenge', express.static('./ssl/.well-known/acme-challenge'));

var startServer = function () {
	if (PRODUCTION) {
		// If an SSL certificate exists, then run the https server
		if (fs.existsSync('/etc/letsencrypt/live/weastie.com/privkey.pem')) {
			// Get SSL certificate
			var ssl = {
				key: fs.readFileSync('/etc/letsencrypt/live/weastie.com/privkey.pem', 'utf8'),
				cert: fs.readFileSync('/etc/letsencrypt/live/weastie.com/fullchain.pem', 'utf8')
			};
			https.createServer(ssl, app).listen(app.get('port'), function () {
				console.log('HTTPS server started on port: ' + app.get('port'));
			});
			// Now, start an HTTP server that will either redirect to HTTPS, or serve SSL files
			var appHttp = express();
			console.log('yeet');
			appHttp.use('/.well-known/acme-challenge', express.static('./ssl/.well-known/acme-challenge'));
			appHttp.all('*', function (req, res) {
				res.redirect('https://www.weastie.com' + req.path);
				res.end();
			});
			appHttp.listen(80, function () {
				console.log('HTTP server started on port 80');
			});
		} else {
			console.log('Error: server started in PRODUCTION mode, but no SSL certificate found');
			app.listen(80, function () {
				console.log('Server started on port: ' + 80);
			});
		}
	} else {
		app.listen(app.get('port'), function () {
			console.log('Server started on port: ' + app.get('port'));
		});
	}
};

// Read sensitive data from private.json
(function parseSecret () {
	var secret = JSON.parse(fs.readFileSync('private.json', 'utf8'));
	secretData.captchaKey = secret.captcha_secret;
	secretData.adminPW = secret.admin_pass;

	app.use(session({
		secret: secret.session_secret,
		resave: true,
		saveUninitialized: false,
		cookie: { secure: PRODUCTION }
	}));

	mongoClient.connect(secret.databaseURL, function (err, client) {
		assert.equal(null, err, 'Error: Failed to connect to database: ' + err);
		var db = client.db('weastie');
		db.createCollection('listeners', function (err, collection) {
			assert.equal(null, err, 'Error: failed to open \'listeners\' collection: ' + err);
			mCols.listeners = collection;
			// Make sure that all listeners delete after 48 hours (2 days)
			mCols.listeners.createIndex({'createdAt': 1}, {expireAfterSeconds: 3600 * 48});
		});

		db.createCollection('html_pages', function (err, collection) {
			assert.equal(null, err, 'Error: failed to open \'html_pages\' collection: ' + err);
			mCols.html_pages = collection;
			// Make sure that all html pages delete after 48 hours (2 days)
			mCols.html_pages.createIndex({'createdAt': 1}, {expireAfterSeconds: 3600 * 48});
		});

		mCols.blogs = db.collection('blogs');
		mCols.analytics = db.collection('analytics');

		console.log('Successfully connected to database...');
		// Once the DB connection is complete, we are ready to start the server
		startServer();
	});
})();

// Function for simplifying the page rendering process
var renderTempl = function (file, req, res, extra) {
	extra = extra || {};
	if (!extra.hasOwnProperty('alerts')) {
		extra.alerts = [];
	}
	// Render the file, and send extra data such as alerts
	res.render(file, {...{session: req.session}, ...extra});
};

// Shortcut to 404
var render404 = function (req, res) {
	res.status(404);
	renderTempl('core/result', req, res, {error: true, text: 'Error 404: Page not found'});
};

// Robots.txt - Allow all robots
app.get('/robots.txt', function (req, res) {
	res.end('User-agent: *\nDisallow:\n');
});

/*
* Administration
*/
app.post('/admin/login', function (req, res) {
	if (req.body.password && req.body.password === secretData.adminPW) {
		req.session.admin = true;
		res.redirect('/admin');
	} else {
		renderTempl('admin/login', req, res, {fail: true});
	}
});
app.all('/admin*', function (req, res, next) {
	if (req.session.admin) {
		next();
	} else {
		renderTempl('admin/login', req, res, {fail: false});
	}
});
app.get('/admin', function (req, res) {
	renderTempl('admin/home', req, res);
});
app.get('/admin/create_blog', function (req, res) {
	renderTempl('admin/create_blog', req, res);
});
app.post('/admin/create_blog', function (req, res) {
	var id = generateID(7);
	mCols.blogs.insertOne({
		title: req.body.title,
		content: req.body.content,
		tags: req.body.tags,
		date: Date.now(),
		_id: id
	}, function (err) {
		assert.equal(err, null, 'Error when creating blog: ' + err);
		res.redirect('/blog/' + global.essentials.convertID(id) + '/' + global.essentials.stringToPageTitle(req.body.title));
	});
});
app.get('/admin/edit_blog/:_id', function (req, res) {
	mCols.blogs.findOne({_id: Number(req.params._id)}, function (err, doc) {
		assert.equal(err, null, 'Error when finding blog to edit: ' + err);
		renderTempl('admin/edit_blog', req, res, {blog: doc});
	});
});
app.post('/admin/edit_blog/:_id', function (req, res) {
	mCols.blogs.update({_id: Number(req.params._id)}, {$set: {content: req.body.content, title: req.body.title, tags: req.body.tags}}, function (err) {
		assert.equal(err, null, 'Error when editing blog: ' + err);
		res.redirect('/blog/' + global.essentials.convertID(Number(req.params._id)) + '/' + global.essentials.stringToPageTitle(req.body.title));
	});
});
app.get('/admin/upload_img', function (req, res) {
	fs.readdir('./assets/blog/img', function (err, files) {
		assert.equal(err, null, 'Error when finding images for blog image uploads');
		renderTempl('admin/upload_img', req, res, {files: files});
	});
});

app.post('/admin/upload_img', function (req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		assert.equal(err, null, 'Error when parsing form with formidable');
		fs.rename(files.image.path, './assets/blog/img/' + fields.file_name, function (err) {
			assert.equal(err, null, 'Error when moving tmp file');
			res.redirect('/admin/upload_img');
		});
	});
});

// Pages to book with analytics
// app.get('*', function (req, res, next) {
// 	console.log('visit: ' + req.url);
// 	next();
// });

/*
 * General Pages
 */

app.get('/', function (req, res) {
	renderTempl('general/home', req, res);
});

app.get(['/blogs', '/blogs/:_tag'], function (req, res) {
	var query = {};
	if (req.params._tag) {
		query.tags = {$regex: '.*' + req.params._tag + '.*'};
	}
	mCols.blogs.find(query, {content: false}).sort({date: -1}).toArray(function (err, docs) {
		assert.equal(err, null, 'Error when finding blogs: ' + err);
		renderTempl('general/view_blogs', req, res, {blogs: docs, filter: req.params._tag});
	});
});

app.get('/blog/:_id/*', function (req, res) {
	mCols.blogs.findOne({_id: global.essentials.convertID(req.params._id)}, function (err, doc) {
		assert.equal(err, null, 'Error when finding single blog: ' + err);
		if (doc) {
			renderTempl('general/view_blog', req, res, {blog: doc});
		} else {
			renderTempl('core/result', req, res, {error: true, text: 'No blog found with this ID'});
		}
	});
});

/* Hack tools */
app.get('/hack_tools', function (req, res) {
	renderTempl('hack_tools/main', req, res);
});

app.get('/hack_tools/listener', function (req, res) {
	renderTempl('hack_tools/listener/setup', req, res);
});

app.get('/hack_tools/listener/:_id', function (req, res) {
	var id = global.essentials.convertID(req.params._id);
	mCols.listeners.findOne({_id: id}, function (err, doc) {
		assert.equal(err, null, 'Error when searching for listener (1): ' + err);
		if (doc) {
			// Show most recent requests first
			doc.requests.sort(function (a, b) {
				return b.date - a.date;
			});
			renderTempl('hack_tools/listener/listener', req, res, {data: doc, id: req.params._id});
		} else {
			renderTempl('core/result', req, res, {error: true, text: 'Could not find a listener with the id: ' + req.params._id});
		}
	});
});

app.all(['/l/:_id', '/l/:_id/*'], function (req, res) {
	res.redirect('/');
	var id = global.essentials.convertID(req.params._id);
	mCols.listeners.findOne({_id: id}, function (err, doc) {
		assert.equal(err, null, 'Error when searching for listener (2): ' + err);
		if (doc) {
			if (!doc.full) {
				// The doc is not over the limit so we'll add the new data
				var info = {
					date: Date.now(),
					method: req.method,
					ip: req.ip,
					path: req.path,
					body: req.body
				};
				mCols.listeners.update({_id: id}, {$push: {requests: info}}, function (err) {
					assert.equal(err, null, 'Error adding listen data to collection: ' + err);
					// Now let's test the size of it the collection to make sure it isn't too big
					// Max: 100kb
					var size = bson.calculateObjectSize(doc) + bson.calculateObjectSize(info);
					if (size > 100000) {
						mCols.listeners.update({_id: id}, {$set: {full: true}});
					}
				});
			}
		}
	});
});

app.post('/hack_tools/create_listener', function (req, res) {
	// Test captcha
	request({
		url: 'https://www.google.com/recaptcha/api/siteverify',
		method: 'POST',
		form: {
			secret: secretData.captchaKey,
			response: req.body['g-recaptcha-response'],
			remoteip: req.ip
		}
	}, function (err, response, body) {
		assert.equal(err, null, 'Error testing google captcha key: ' + err);
		if (JSON.parse(body).success) {
			// Success!
			var id = generateID(4);
			mCols.listeners.findOne({_id: id}, function (err, doc) {
				// Make sure that the listener does not currently exist
				assert.equal(err, null, 'Error checking if listener exists: ' + err);
				if (doc) {
					res.end('Something super duper rare happened! Try again please...');
				} else {
					mCols.listeners.insertOne({
						_id: id,
						createdAt: new Date(),
						requests: [],
						full: false
					}, function (err) {
						assert.equal(err, null, 'Error creating listener: ' + err);
						res.redirect(path.join('/hack_tools/listener/', global.essentials.convertID(id)));
					});
				}
			});
		} else {
			renderTempl('hack_tools/listener/setup', req, res, {alerts: [{message: 'Please complete captcha', type: 'error'}]});
		}
	});
});

app.get('/hack_tools/html', function (req, res) {
	renderTempl('hack_tools/html', req, res);
});

app.post('/hack_tools/create_html', function (req, res) {
	var id = generateID(9);
	mCols.html_pages.insertOne({
		_id: id,
		createdAt: new Date(),
		content: req.body.content
	}, function (err) {
		assert.equal(err, null, 'Error creating html page: ' + err);
		res.redirect(path.join('/hack_tools/html_page/', global.essentials.convertID(id)));
	});
});

app.get('/hack_tools/html_page/:_id', function (req, res) {
	var id = global.essentials.convertID(req.params._id);
	mCols.html_pages.findOne({_id: id}, function (err, doc) {
		assert.equal(err, null, 'Error finding html page: ' + err);
		if (doc) {
			res.end(doc.content);
		} else {
			render404(req, res);
		}
	});
});

app.get('/hack_tools/converter', function (req, res) {
	renderTempl('hack_tools/converter', req, res);
});

/* Music tools */
app.get('/music_tools/', function (req, res) {
	renderTempl('music_tools/main', req, res);
});

app.get('/music_tools/fretboard', function (req, res) {
	renderTempl('music_tools/fretboard', req, res);
});

// Brashbrawl
app.get('/brashbrawl', function (req, res) {
	renderTempl('games/brashbrawl', req, res);
});

// Alan turing ENG15 project
app.get('/alan_turing', function (req, res) {
	renderTempl('projects/alan_turing/home.pug', req, res);
});
app.get('/alan_turing/ww2', function (req, res) {
	renderTempl('projects/alan_turing/ww2.pug', req, res);
});
app.get('/alan_turing/tragedy', function (req, res) {
	renderTempl('projects/alan_turing/tragedy.pug', req, res);
});
app.get('/alan_turing/today', function (req, res) {
	renderTempl('projects/alan_turing/today.pug', req, res);
});
app.get('/alan_turing/bib', function (req, res) {
	renderTempl('projects/alan_turing/bib.pug', req, res);
});

// Given an object with properties, make sure each property is of correct type
// Form: an object with properties and values
// Obj: an object with properties that form should have, and their types
function validateForm (form, obj) {
	for (var key in obj) {
		if (form.hasOwnProperty(key)) {
			if (toType(form[key]) !== obj[key]) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

// If none of the above routings worked, then we throw a 404
app.use(function (req, res, next) {
	render404(req, res);
});

// Returns a number to be used in base 32 for ID storage
function generateID (numLetters) {
	let b32Lower = '';
	let b32Upper = '';
	for (let i = 0; i < numLetters; i++) {
		b32Lower += '0'; // Lowest base32 ascii
		b32Upper += 'v'; // Highest base32 ascii
	}
	var lowerBound = Number.parseInt(b32Lower, 32);
	var upperBound = Number.parseInt(b32Upper, 32);
	return Math.floor(Math.random() * (upperBound - lowerBound)) + lowerBound;
}

// Function from stack overflow: https://stackoverflow.com/questions/7390426/better-way-to-get-type-of-a-javascript-variable#7390612
function toType (obj) {
	return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
}
